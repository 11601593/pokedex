import {Directive, ElementRef, HostListener, Input} from '@angular/core';

@Directive({
  selector: '[appBorderCard]'
})
export class BorderCardDirective {

  private GREY_COLOR = 'grey';
  private RED_COLOR = 'red';
  private GREEN_COLOR = 'green';
  @Input('appBorderCard') borderColor;

  constructor(private element: ElementRef) {
    this.setBorder(this.GREY_COLOR);
    this.setHeight(240);
    this.onMouseEnter();
    this.onMouseLeave();
  }

  private setBorder(color: string): void{
    const border = 'solid 2px ' + color;
    this.element.nativeElement.style.border = border;
  }

  private setHeight(height: number): void{
    this.element.nativeElement.style.height = height + 'px';
  }

  @HostListener('mouseenter') onMouseEnter(){
    this.setBorder(this.borderColor || this.GREEN_COLOR);
  }

  @HostListener('mouseleave') onMouseLeave(){
    this.setBorder(this.GREY_COLOR);
  }

}
