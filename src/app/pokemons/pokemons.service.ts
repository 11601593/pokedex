import { Injectable } from '@angular/core';
import {POKEMONS} from '../shared/list.pokemons';
import {Pokemon} from '../pokemon';

@Injectable({
  providedIn: 'root'
})
export class PokemonsService {

  constructor() { }

  getListPokemons(): Pokemon[]{
    return POKEMONS;
  }

  getSinglePokemon(id: number): Pokemon{
    const listPkm = this.getListPokemons();
    for(let i = 0; i < listPkm.length; i++){
      if (id === listPkm[i].id){
        return listPkm[i];
      }
    }
  }

  getPokemonType(): string[]{
    return ['Plante', 'Feu', 'Eau', 'Insecte', 'Normal', 'Electrique', 'Poison', 'Fée', 'Vol'];
  }
}
