import { Component, OnInit } from '@angular/core';
import {Pokemon} from '../../pokemon';
import {ActivatedRoute} from '@angular/router';
import {Router} from '@angular/router';
import {PokemonsService} from '../pokemons.service';

@Component({
  selector: 'app-detail-pokemon',
  templateUrl: './detail-pokemon.component.html',
  styleUrls: ['./detail-pokemon.component.scss']
})
export class DetailPokemonComponent implements OnInit {
  listOfPokemons: Pokemon[];
  pokemonToDisplay: Pokemon;
  constructor(private route: ActivatedRoute, private router: Router, private pokemonsService: PokemonsService) {
    this.listOfPokemons = this.pokemonsService.getListPokemons();
  }

  ngOnInit(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    // tslint:disable-next-line:prefer-for-of
    // for (let i = 0; i < this.listOfPokemons.length; i++){
    //   if (this.listOfPokemons[i].id === id){
    //     this.pokemonToDisplay = this.listOfPokemons[i];
    //   }
    // }
    this.pokemonToDisplay = this.pokemonsService.getSinglePokemon(id);
  }

  retour(): void{
    const link = [''];
    this.router.navigate(link);
  }

  editerPokemon(pokemonToEdit: Pokemon): void{
    const link = ['/pokemon/edit', pokemonToEdit.id];
    this.router.navigate(link);
  }

}
